import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import { exec } from 'child_process';
import * as util from 'util';
import * as SAATC_UTIL from '../../ssatc-util';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Login extends SfdxCommand{
    public static description = messages.getMessage('login.description');
    public static examples = [
        messages.getMessage('login.example1'), 
        messages.getMessage('login.example2')
    ];
    protected saatcUtil: SAATC_UTIL.SAATC_UTIL = new SAATC_UTIL.SAATC_UTIL;
    protected static flagsConfig = {
        alias : flags.string({
            char : 'a',
            description : messages.getMessage('login.params.alias.description')
        }),
        target : flags.string({
            char : 't',
            description : messages.getMessage('login.params.target.description')
        }),
        default : flags.boolean({
            char : 'D',
            description : messages.getMessage('login.params.default.description')
        })
    }

    protected static requiresUsername = false;

    //protected static supportsDevhubUsername = true;
    protected static requiresProject = false;

    public async run(): Promise<AnyJson>{
        const asyncexec = util.promisify(exec),
                alias = this.flags.alias,
                target = this.flags.target,
                isDefault = this.flags.default ?? false;
        let fullFile: AnyJson = await this.saatcUtil.getConfigFile(),
            uri = this.flags.uri,
            savedPaths: JSON = fullFile['savedPaths'];
        let selectedPath: JSON;
        if(target){
            selectedPath = savedPaths[target];
            if(selectedPath === undefined){
                let addNew = await this.ux.prompt(messages.getMessage('login.prompt.addNewConfig'))
                while(uri === undefined) uri = await this.ux.prompt(messages.getMessage('login.prompt.addNewConfig.addNewURI'))
                if(addNew.toLowerCase() === 'y'){ 
                    fullFile = await this.saatcUtil.addConfig({target, uri, isDefault});
                    selectedPath = fullFile['savedPaths'][target];
                }
                else{
                    const exitMessage = messages.getMessage('login.abort.configNotAdded')
                    this.ux.log(exitMessage)
                    return exitMessage;
                }
            }
            else if(isDefault){
                this.saatcUtil.configureDefault(savedPaths, target);
                await this.saatcUtil.writeConfigFile(fullFile);
            }
        }
        else{
            if(Object.entries(savedPaths).length == 0){
                throw new SfdxError('No default target saved, add one first')
            }
            selectedPath = Object.values(savedPaths).find(element => element['isDefault'])
        }
        let command = `sfdx force:org:open -p ${selectedPath['uri']}`;
        if(alias) command = command + ` -u ${alias}`;
        let {stdout, stderr} = await asyncexec(command);
        this.ux.log(stderr);
        this.ux.log(stdout);
        return selectedPath['uri'];
    }
}
