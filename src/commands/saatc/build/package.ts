import { flags, SfdxCommand } from '@salesforce/command';
import { Messages } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import * as SAATC_UTIL from '../../../ssatc-util';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Package extends SfdxCommand{
    public static description = messages.getMessage('login.description');
    public static examples = [
        messages.getMessage('login.example1'), 
        messages.getMessage('login.example2')
    ];
    
    protected static flagsConfig = {
        packagename : flags.filepath({
            char : 'p',
            description : messages.getMessage('login.add.params.target.description'),
            required : true
        }),
        api : flags.string({
            char : 'v',
            description : 'API version for the built package if different than what is already declared',
        }),
        commit: flags.string({
            char: 'c',
            description: 'Add the changes specified by the commit hash to the package.xml'
        })
    }

    protected static requiresUsername = false;
    protected saatcUtil: SAATC_UTIL.SAATC_UTIL = new SAATC_UTIL.SAATC_UTIL;

    //protected static supportsDevhubUsername = true;
    protected static requiresProject = false;

    public async run(): Promise<AnyJson>{
        this.ux.startSpinner('Building package...');
        const path:string = this.flags.packagename,
            apiversion:string = this.flags.api,
            commitHash:string = this.flags.commit;
        let result = await this.saatcUtil.getWorkingJSONPackage(path, apiversion, commitHash);
        this.ux.stopSpinner('Package complete');
        if(result?.status == 1){
            return ({
                status: 1,
                result: 'Package failed to compile'
            })
        }
        this.saatcUtil.convertJSON2XML(JSON.stringify(result.result), path);
        return result.result;
    }

    
}