import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core'; //ConfigAggregator,
import { AnyJson } from '@salesforce/ts-types';
//import * as os from '@oclif/command';
import { exec } from 'child_process';
import * as util from 'util';
import {SAATC_UTIL} from '../../ssatc-util'

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Deploy extends SfdxCommand{
    public static description = messages.getMessage('deploy.description');
    public static examples = [
        messages.getMessage('deploy.example1'), 
        messages.getMessage('deploy.example2')
    ];
    
    protected saatcUtil = new SAATC_UTIL();
    protected static flagsConfig = {
        fulldeploy : flags.boolean({
            char:'F', 
            description : messages.getMessage('deploy.params.full.description')
        }),
        packagename : flags.string({
            char:'p', 
            description : messages.getMessage('deploy.params.packageXML.description'), 
            dependsOn : ['fulldeploy']
        }),
        commit: flags.string({
            char: 'c',
            description: 'Add the changes specified by the commit hash to the package.xml'
        })
    }

    protected static requiresUsername = false;

    //protected static supportsDevhubUsername = true;
    protected static requiresProject = true;

    public async run(): Promise<AnyJson>{
        const asyncexec = util.promisify(exec),
            doFullDeploy = this.flags.fulldeploy,
            packagename = this.flags.packagename,
            commitHash:string = this.flags.commit;

        this.ux.startSpinner('Deploying source to org');
        if(doFullDeploy){
            if(!packagename)
                throw new SfdxError('Please provide the location for the package.xml to deploy from');
            
            let {stdout, stderr} = await asyncexec(`sfdx force:source:deploy --manifest="./manifest/${packagename}"`)
            this.ux.stopSpinner();
            this.ux.log(stderr);
            this.ux.log(stdout);
            return {result: stdout}
        }
        else{
            let result = await this.saatcUtil.getForceAppModifiedFiles();
            if(result.status == 1){
                this.ux.stopSpinner();
                throw new SfdxError(result.error);
            }
            
            let filesToDeploySet = new Set<string>(result.result.additions);
            if(commitHash){
                let getCommitFiles = await this.saatcUtil.getModifiedFilesFromCommitHash(commitHash);
                if(getCommitFiles.status == 1){
                    this.ux.stopSpinner();
                    throw new SfdxError(result.error);
                }
                getCommitFiles.result.additions.forEach(element => {
                    filesToDeploySet.add(element);
                })
            }
            let filesToDeploy = [];
            for(const element of filesToDeploySet.values()){
                filesToDeploy.push(element);
            }
            var {stdout, stderr} = await asyncexec(`sfdx force:source:deploy -p ${filesToDeploy.join(',')}`);
            this.ux.log(stderr);
            this.ux.log(stdout);
            return({result: stdout});
            
        }
    }
}
