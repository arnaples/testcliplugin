import { SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import * as SAATC_UTIL from '../../ssatc-util';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Add extends SfdxCommand{
    public static description = messages.getMessage('login.description');
    public static examples = [
        messages.getMessage('login.example1'), 
        messages.getMessage('login.example2')
    ];

    protected static requiresUsername = false;
    protected saatcUtil: SAATC_UTIL.SAATC_UTIL = new SAATC_UTIL.SAATC_UTIL;

    //protected static supportsDevhubUsername = true;
    protected static requiresProject = false;

    public async run(): Promise<AnyJson>{
        let configFile = await this.saatcUtil.getConfigFile();
        let agileOrg = configFile['AgileAccelerator.Org.Alias'];
        if(agileOrg === undefined){
            throw new SfdxError(
                'No org alias has been set for the agile accelerator configuration',
                'Alias not set',
                [
                    'sfdx saatc:agile:configure -a yourAuthorizedAgileAcceleratorOrgAlias'
                ],
                1
            )
        }
        const message = `The org alias for the agile accelerator org is set to ${agileOrg}`;
        this.ux.log(message);
        return {
            status: 0,
            result: message
        }
    }
}