import { flags, SfdxCommand } from '@salesforce/command';
import { Messages } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import * as SAATC_UTIL from '../../../ssatc-util';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Add extends SfdxCommand{
    public static description = messages.getMessage('login.description');
    public static examples = [
        messages.getMessage('login.example1'), 
        messages.getMessage('login.example2')
    ];
    
    protected static flagsConfig = {
        target : flags.string({
            char : 't',
            description : messages.getMessage('login.add.params.target.description'),
            required : true
        }),
        uri : flags.string({
            char : 'u',
            description : messages.getMessage('login.add.params.uri.description'),
            required : true
        }),
    }

    protected static requiresUsername = false;
    protected saatcUtil: SAATC_UTIL.SAATC_UTIL = new SAATC_UTIL.SAATC_UTIL;

    //protected static supportsDevhubUsername = true;
    protected static requiresProject = false;

    public async run(): Promise<AnyJson>{
        const target:string = this.flags.target,
            isDefault = false,
            uri:string = this.flags.uri;
        let fileWrittenSuccessfully: AnyJson = false;
        fileWrittenSuccessfully = await this.saatcUtil.addConfig({target, uri, isDefault})
        if(fileWrittenSuccessfully === false){
            var result = await this.ux.prompt('The provided target already exists, would you like to overwrite with the new info? [y/n]');
            if(result.toLowerCase() === 'y') fileWrittenSuccessfully = await this.saatcUtil.addConfig({target, uri, isDefault}, true);
        }
        if(fileWrittenSuccessfully === false) return 'Addition skipped';
        let message = `Added the configuration for ${target}`;
        this.ux.log(message);
        return message;
    }
}
