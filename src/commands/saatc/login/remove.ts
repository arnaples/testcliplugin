import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import * as SAATC_UTIL from '../../../ssatc-util';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Remove extends SfdxCommand{
    public static description = messages.getMessage('login.description');
    public static examples = [
        messages.getMessage('login.example1'), 
        messages.getMessage('login.example2')
    ];
    
    protected static flagsConfig = {
        target : flags.string({
            char : 't',
            description : messages.getMessage('login.remove.params.target.description'),
            required : true
        })
    }
    protected saatcUtil: SAATC_UTIL.SAATC_UTIL = new SAATC_UTIL.SAATC_UTIL;
    protected static requiresUsername = false;

    //protected static supportsDevhubUsername = true;
    protected static requiresProject = false;

    public async run(): Promise<AnyJson>{
        const target = this.flags.target;
        let fullFile = await this.saatcUtil.getConfigFile(),
            savedPaths:Object = fullFile['savedPaths'];
        if(savedPaths[target] === undefined) throw new SfdxError(messages.getMessage('login.remove.error.targetDoesNotExist'));

        delete savedPaths[target]
        await this.saatcUtil.writeConfigFile(fullFile);
        let message = messages.getMessage('login.remove.finished').replace('${target}',target);
        this.ux.log(message);
        return message;
    }
}

