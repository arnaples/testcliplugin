import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import * as SAATC_UTIL from '../../../ssatc-util';
import { exec } from 'child_process';
import * as util from 'util';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Components extends SfdxCommand{
    public static description = messages.getMessage('login.description');
    public static examples = [
        messages.getMessage('login.example1'), 
        messages.getMessage('login.example2')
    ];
    
    protected static flagsConfig = {
        packagename : flags.filepath({
            char : 'p',
            description : messages.getMessage('login.add.params.target.description'),
            required : true
        }),
        manualconfigurations : flags.boolean({
            char: 'M',
            description: 'FEATURE NOT IMPLEMENTED If set, will start an interactive session to write manual config steps'
        }),
        workname: flags.string({
            char: 'w',
            description: 'The work Id to associate this package to',
            required: true
        }),
        commit: flags.string({
            char: 'c',
            description: 'Add the changes specified by the commit hash to the package.xml'
        })

    }
    protected refId = 0;
    protected static requiresUsername = false;
    protected saatcUtil: SAATC_UTIL.SAATC_UTIL = new SAATC_UTIL.SAATC_UTIL;

    //protected static supportsDevhubUsername = true;
    protected static requiresProject = true;

    public async run(): Promise<AnyJson>{
        this.ux.startSpinner('Building package...');
        const path:string = this.flags.packagename,
            workName:string = this.flags.workname,
            commitHash:string = this.flags.commit,
            config = await this.saatcUtil.getConfigFile()
        if(! config?.['AgileAccelerator.Org.Alias']){
            throw new SfdxError(
                'agile accelerator org not set',
                'config not set',
                [
                    'sfdx saatc:agile:configure -a myTrainingOrg'
                ]
            )
        }
        const agileAlias = config['AgileAccelerator.Org.Alias'];
        let getWorkIdResult = await this.getWorkId(workName, agileAlias),
            workId;
        if(getWorkIdResult['status'] == 1){
            throw new SfdxError(`Error retrieving results. Erros: ${getWorkIdResult['result']}`)
        }
        else{
            workId = getWorkIdResult['result']?.['result']?.['records']?.[0]?.['Id'];
        }
        let packageJSON = await this.saatcUtil.getWorkingJSONPackage(path, undefined, commitHash),
            types = packageJSON.result?.['Package']?.[0]?.['types'] ?? []
        let treeBody = {records:[]};
        types.forEach(type => {
            type['members'].forEach(member => {
                treeBody.records.push(this.getComponentReferenceTreeBody(workId, member, type['name']))
            });
        })
        let treePath = './config/saatc/treeImport';
        await this.saatcUtil.writeJSONFile(treeBody, treePath);
        let treeImportResponse = await this.importTree(treePath, agileAlias);
        if(treeImportResponse['status'] == 1){
            throw new SfdxError(treeImportResponse['result']);
        }
        const message = `Components have been added to ${workName}`;
        this.ux.log(message);
        return {
            status: 0,
            result: message
        }
    }
    private getComponentReferenceTreeBody(workId:string, componentAPI:string, componentType:string){
        return {
            attibutes:{
                type: "Release_Component__c",
                referenceId: this.refId++
            },
            Bug_Story__c: workId,
            Component_API_Name__c: componentAPI,
            Component_Type__c: componentType,
            Developer_s_Description__c: 'Created using the saatc sfdx-cli plugin',
            Manual_Configuration__c: false,
            Manual_Configuration_Notes__c: ''
        }
    }
    private async getWorkId(workName:string, agileAlias:string):Promise<AnyJson>{
        let asyncexec = util.promisify(exec),
            {stdout, stderr} = await asyncexec(`sfdx force:data:soql:query -q "SELECT Id FROM agf__ADM_Work__c WHERE Name = '${workName}'" -u ${agileAlias} --json`),
            response = {status: 0, result:''};
        if(stderr){
            response.status = 1,
            response.result = stderr;
        }
        else{
            response.result = stdout?.['result']?.['records']?.[0]?.['Id'];
        }
        return response;
    }
    private async importTree(filepath:string, agileAlias:string):Promise<AnyJson>{
        let asyncexec = util.promisify(exec),
            {stdout, stderr} = await asyncexec(`sfdx force:data:tree:import -f ${filepath} -u ${agileAlias}`),
            response = {status: 0, result:''};
        if(stderr){
            response.status = 1,
            response.result = stderr;
        }
        else{
            response.result = stdout;
        }
        return response;
    }
}