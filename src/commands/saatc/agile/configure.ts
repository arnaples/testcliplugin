import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import * as SAATC_UTIL from '../../../ssatc-util';
import { exec } from 'child_process';
import * as util from 'util';

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('saatc-sfdx', 'saatc');

export default class Add extends SfdxCommand{
    public static description = messages.getMessage('login.description');
    public static examples = [
        messages.getMessage('login.example1'), 
        messages.getMessage('login.example2')
    ];
    
    protected static flagsConfig = {
        alias : flags.string({
            char : 'a',
            description : messages.getMessage('login.add.params.target.description'),
            required : true
        }),
    }

    protected static requiresUsername = false;
    protected saatcUtil: SAATC_UTIL.SAATC_UTIL = new SAATC_UTIL.SAATC_UTIL;

    //protected static supportsDevhubUsername = true;
    protected static requiresProject = false;

    public async run(): Promise<AnyJson>{
        const targetAlias:string = this.flags.alias,
            asyncexec = util.promisify(exec);

        let {stdout, stderr} = await asyncexec('sfdx auth:list --json');
        let isTargetAliasAuthorized = false;
        if(stderr){
            console.log(`Error reported ${stderr}`)
        }
        let authResult : JSON = JSON.parse(stdout);
        if(authResult?.['status'] == 0){
            let results: Array<JSON> = authResult['result'];
            isTargetAliasAuthorized = results.some(element => element['alias'] == targetAlias);
        }
        if(!isTargetAliasAuthorized){
            throw new SfdxError(
                'No authorized org exists with the supplied org alias. Check for spelling or authorize a new org',
                'Unknown target alias',
                [
                    `sfdx auth:web:login -a ${targetAlias} -r https://mydomain.my.salesforce.com`
                ],
                1
            )
        }
        let configFile = await this.saatcUtil.getConfigFile();
        configFile['AgileAccelerator.Org.Alias'] = targetAlias;
        await this.saatcUtil.writeConfigFile(configFile);

        const message = `Org alias for the agile accelerator org has been set to ${targetAlias}`;
        this.ux.log(message);
        return {
            status: 0,
            result: message
        }
    }
}