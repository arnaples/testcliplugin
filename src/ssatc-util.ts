import * as path from 'path';
import {core} from '@salesforce/command';
import { AnyJson } from '@salesforce/ts-types';
import * as xml_js from 'xml-js';
import { exec } from 'child_process';
import * as util from 'util';
import source2manifest from './source2manifest';
export interface CONFIG_SCHEMA {
    target: string,
    uri: string,
    isDefault: boolean
}
export class SAATC_UTIL {
    
    private filePath = "./config/saatc/saatc-config.json";
    private addedToGitIgnore = false;
    public async getConfigFile(): Promise<AnyJson>{
        let filePath = path.resolve(this.filePath);

        let fullFile;
        if(await core.fs.fileExists(this.filePath)){
           fullFile = await core.fs.readJson(filePath);
           this.addedToGitIgnore = fullFile['gitIgnored'] ?? false;
        }
        else{
            //File doesn't exist at path. Must create a new one before proceeding
            const basicStructure = {"savedPaths":{},"gitIgnored":false};
            this.writeConfigFile(basicStructure);
            fullFile = basicStructure;
        }
        return fullFile;
    }
    public async writeConfigFile(jsonStructure:AnyJson): Promise<void>{
        if(! this.addedToGitIgnore){
            let theFile = (await core.fs.readFile('./.gitignore')).toString().split('\n');
            if(!theFile.includes('config/saatc')){
                theFile.push('config/saatc');
                core.fs.writeFile('./.gitignore',theFile.join('\n'));
            }
        }
        this.writeJSONFile(jsonStructure, this.filePath);
    }
    public async writeJSONFile(jsonStructure:AnyJson, filePath:string): Promise<void>{
        await core.fs.writeJson(filePath, jsonStructure);
    }
    public async addConfig(schema: CONFIG_SCHEMA, forceAdd?: boolean): Promise<AnyJson>{
        let fullFile = await this.getConfigFile(),
            {target, uri, isDefault} = schema,
            savedPaths:JSON = fullFile['savedPaths'];
        if(savedPaths[target] !== undefined && !forceAdd){
            return false;
        }
        if(uri.startsWith('/')) uri = uri.replace('/','');
        savedPaths[target] = {
            uri: uri,
            isDefault: isDefault
        }
        if(isDefault) this.configureDefault(savedPaths, target)
        await this.writeConfigFile(fullFile);
        return fullFile;
    }
    public configureDefault(targetStructure: JSON, targetToDefaul: string): JSON{
        for(let value in targetStructure){
            targetStructure[value]['isDefault'] = false;
        }
        targetStructure[targetToDefaul]['isDefault'] = true;
        return targetStructure;
    }
    public convertXML2JSON(xmlFilePath:string): JSON{
        if(!core.fs.fileExistsSync(xmlFilePath)){
            core.fs.writeFileSync(xmlFilePath, undefined, {encoding:'utf-8'});
            return JSON.parse('{}');
        }
        let xml = core.fs.readFileSync(xmlFilePath, 'utf-8');
        const options = {
            ignoreComment: true,
            alwaysChildren: true,
            alwaysArray: true,
            compact: true
        };
        let result:JSON = JSON.parse(xml_js.xml2json(xml, options));
        return result;
    }
    public convertJSON2XML(jsonObject:string, xmlFilePath:string):any{
        let xml = xml_js.json2xml(jsonObject, {compact:true});
        core.fs.writeFileSync(xmlFilePath, xml, {encoding:'utf-8'});
    }
    public async getForceAppModifiedFiles(){
        
        const asyncexec = util.promisify(exec);
        var {stdout, stderr} = await asyncexec('git status --porcelain=v2 -z');
            if(stderr){
                return {status:1, error: stderr};
            }
            else{
                const changedFiles = stdout.split(String.fromCharCode(0));
                let additionPaths:Array<string> = [],
                deletionPaths:Array<string> = [],
                    type:string, xy:string, sub:string, mH:string, 
                    mI:string, mW:string, hH:string, hI:string, 
                    gitPath:Array<string>; 
                changedFiles.forEach(changedFile => {
                    if(changedFile){
                        switch (changedFile.charAt(0)){
                            case '1': 
                                [type, xy, sub, mH, mI, mW, hH, hI, ...gitPath] = changedFile.split(' ');
                                break;
                            case '?':
                            case '!':
                                [type, ...gitPath] = changedFile.split(' ');
                                break;
                        }
                        type = type; sub = sub; mH = mH; mI = mI; mW = mW; hH = hH; hI = hI;
                        
                        //checking if item is in force-app dir
                        let condensedPath = gitPath.join(' ');
                        condensedPath = condensedPath.substring(condensedPath.indexOf('/')+1); //gets rid of the file directory
                        if(condensedPath.startsWith('force-app') || condensedPath.startsWith('src')){
                            let _unused = (xy.includes('A') || xy.includes('M')) ? additionPaths.push(condensedPath) : xy.includes('D') ? deletionPaths.push(condensedPath) : undefined;
                            _unused = _unused
                        }
                    }
                });
                return({status: 0, result: {additions: additionPaths, deletions: deletionPaths}});
            }
    }
    public async getModifiedFilesFromCommitHash(commitHash:string){
        const asyncexec = util.promisify(exec);
        var {stdout, stderr} = await asyncexec(`git diff ${commitHash}~ ${commitHash} --name-status -z`);
            if(stderr){
                return {status:1, error: stderr};
            }
            else{
                const changedFiles = stdout.split(String.fromCharCode(0));
                let additionPaths:Array<string> = [],
                    deletionPaths:Array<string> = [],
                    type:string, gitPath:Array<string>; 
                changedFiles.forEach(changedFile => {
                    if(changedFile){
                        [type, ...gitPath] = changedFile.split(' ');
                        let condensedPath = gitPath.join(' ');
                        //checking if item is in force-app dir
                        condensedPath = condensedPath.substring(condensedPath.indexOf('/')+1); //gets rid of the file directory
                        if(condensedPath.startsWith('force-app') || condensedPath.startsWith('src')){

                            ['A','M'].includes(type) ? additionPaths.push(condensedPath) : deletionPaths.push(condensedPath);
                        }
                    }
                });
                return({status: 0, result: {additions: additionPaths, deletions: deletionPaths}});
            }
    }
    public async getWorkingJSONPackage(path:string, apiversion?:string, commitHash?:string){
        let xmlJSON:JSON = this.convertXML2JSON(path);
        let packageJSON:JSON = xmlJSON['Package'];
        let betterJSON = {
            _declaration:{
                _attributes : {
                    version:'1.0',
                    encoding:"UTF-8",
                    standalone:"yes"
                }
            },
            Package:[
                {
                    _attributes:{
                        xmlns:'http://soap.sforce.com/2006/04/metadata'
                    },
                    types:[],
                    version: apiversion ?? packageJSON?.[0]?.['version']?.[0]?.['_text']?.[0] ?? '49.0' //if for some reason the api version isn't declared or already set in the package, default it to 49 (GA as of this file)
                }
            ]
        }
        //build the change log structure from the existing package.xml
        betterJSON.Package[0].types = packageJSON?.[0]?.['types']?.map(element => {
            let members: Array<string> = element['members'] ? element['members'].map(iElement => iElement['_text'][0]) : [];
            return {
                name: element['name']?.[0]?.['_text']?.[0] ?? 'Unnamed',
                members: members
            }
        }) ?? [];

        //add new working files to the change
        let getModifiedFiles = await this.getForceAppModifiedFiles();
        if(getModifiedFiles.status == 1){
            return ({
                status: 1,
                result: 'something failed'
            })
        }
        else{
            this.handleFiles(getModifiedFiles.result, betterJSON)
        }

        //finally if a commit has was provided, add changes from commit
        if(commitHash){
            let committedFiles = await this.getModifiedFilesFromCommitHash(commitHash);
            if(committedFiles.status == 1){
                return ({
                    status: 2,
                    result: 'something failed'
                })
            }
            else{
                this.handleFiles(committedFiles.result, betterJSON)
            }
        }
        
        this.cleanTypes(betterJSON.Package[0].types);
        return ({
            status: 0,
            result: betterJSON
        })
    }
    private funkyFileNames = ['reports', 'dashboards', 'emails', 'documents'];
    private handleFiles(filePaths, betterJSON){
        for(let entry of filePaths.additions){
            let meta_API = entry.split('main/default/')[1],
                [meta, API, ...otherAttributes] = meta_API.split('/');
            this.addToStructure(betterJSON.Package[0].types, meta, API, otherAttributes)
        }
        for(let entry of filePaths.deletions){
            let meta_API = entry.split('main/default/')[1],
                [meta, API, ...otherAttributes] = meta_API.split('/');
            this.removeFromStructure(betterJSON.Package[0].types, meta, API, otherAttributes)
        }
    }
    private addToStructure(typeList:Array<object>, node:string, value:string, ...otherAttributes){
        const manifestType:string = source2manifest[node];
        typeList = typeList ?? []; //if for some reason this is undefined
        let thisType = typeList.find(thisType => thisType['name'] == manifestType);
        if(thisType === undefined){
            typeList.push({
                name: manifestType,
                members: []
            })
        }
        let typeMembers = thisType['members'];
        if(this.funkyFileNames.includes(node)) value = this.formatFileName(value, otherAttributes);
        value = this.cleanFileName(value);
        if(!typeMembers.includes(value)) typeMembers.push(value);
    }
    private removeFromStructure(typeList:Array<object>, node:string, value:string,  ...otherAttributes){
        const manifestType:string = source2manifest[node];
        typeList = typeList ?? []; //if for some reason this is undefined
        let thisTypeIndex = typeList.findIndex(thisType => thisType['name'] == manifestType),
            thisType = typeList[thisTypeIndex];
        if(thisType === undefined){
            typeList.push({
                name: manifestType,
                members: []
            })
        }
        let typeMembers = thisType['members'];
        if(this.funkyFileNames.includes(node)) value = this.formatFileName(value, otherAttributes);
        value = this.cleanFileName(value);
        if(typeMembers.includes(value)) typeMembers.splice(typeMembers.indexOf(value), 1);
        if(typeMembers.length == 0) typeList.splice(thisTypeIndex, 1);
    }
    private formatFileName(value:string, ...otherAttributes):string{
        otherAttributes.splice(0, 0, value);
        return otherAttributes.join('/');
    }
    private cleanFileName(value:string):string{
        let fileParts = value.split(/^([\w- ]+(?=\.))(?:\.\w+-meta)?(?:\.\w+$)/);
        return fileParts.length == 1 ? fileParts[0] : fileParts[1];
    }
    private cleanTypes(typeArray:Array<string>){
        typeArray.forEach(element => {
            element['members'].sort( (a:string, b:string)=> {
                a = a.toLowerCase();
                b = b.toLowerCase();
                return a < b ? -1 : a == b ? 0 : 1;
            })
        });
        typeArray.sort((a,b) => {
            const aname = a['name'], bname = b['name'];
            return aname < bname ? -1 : aname == bname ? 0 : 1;
        })
    }
}